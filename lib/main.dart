import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Sample Images'),
    );
  }
}

List<String> images = new List();
int click = 0;
getImage() async {
  if (click == 1) {
    final response = await http.get(
        "https://pixabay.com/api/?key=14448279-e74cbb27d7a35f52337d61dd1&per_page=200");
    final imageJson = json.decode(response.body);

    images.add(imageJson['hits'][ran.nextInt(200)]['largeImageURL'].toString());
  }
  click = 0;
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

Random ran = new Random();

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                click = 1;
                setState(() {});
              },
            )
          ],
        ),
        body: FutureBuilder<dynamic>(
          future: getImage(), // a previously-obtained Future<String> or null
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Text('Press button to start.');
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Center(
                    child: CircularProgressIndicator(
                  backgroundColor: Colors.blue,
                ));
              case ConnectionState.done:
                if (snapshot.hasError) return Text('Error: ${snapshot.error}');
                return ListView.builder(
                  itemCount: images.length,
                  itemBuilder: (BuildContext context, int i) {
                    return GestureDetector(
                      child: Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 10.0),
                        height: 200.0,
                        //color: Colors.red,
                        child: Image.network(images[i], fit: BoxFit.cover),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailImage(image: images[i]),
                          ),
                        );
                      },
                    );
                  },
                );
            }
            return null; // unreachable
          },
        ));
  }
}

class DetailImage extends StatelessWidget {
  String image;

  DetailImage({Key key, @required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Center(
                  child: GestureDetector(
            child: Image.network(image),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ));
  }
}
